# This repository is deprecated. Please use [BImEnc2](https://gitlab.com/flexagoon/bimenc2) instead.

# BImEnc
A demonstration of basic number → image encryption.

## Dependencies
- numpy
- pillow

## Usage
``` bash
# Encrypting a number
python main.py -n <number> -f <filename>

# Decrypting an image
python main.py -f <filename>

# You can also use the -i flag to enable invisible mode.
# Invisible mode images look completely white to human eye,
# But they can become undecryptable if compressed in size.
python main.py -n <number> -f <filename> -i
python main.py -f <filename> -i
```
