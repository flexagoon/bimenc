#!/usr/bin/env python3
"""Encrypts and decrypts numbers.

Functions:

encode(<string with a number>)
decode(<list of numbers generated by encode()>)"""


def encode(number):
    """Encode a number."""
    encoded = [int(number[0])]
    for i in range(1, len(number)):
        current_digit = int(number[i])
        previous_digit = int(number[i - 1])
        if current_digit >= previous_digit:
            encoded.append(current_digit - previous_digit)
        else:
            encoded.append(current_digit + 10 - previous_digit)
    return encoded


def decode(number):
    """Decode a number."""
    decoded = [str(number[0])]
    for i in range(1, len(number)):
        current_digit = number[i]
        previous_digit = int(decoded[i - 1])
        if current_digit + previous_digit < 10:
            decoded.append(str(current_digit + previous_digit))
        else:
            decoded.append(str(current_digit + previous_digit - 10))
    return "".join(decoded)
