#!/usr/bin/env python3

import argparse

from modules import number_processor as np
from modules import image_processor as ip

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Encrypt and decrypt numbers into images",
        epilog="Make sure to star the project on GitLab if you like it!",
    )

    parser.add_argument(
        "-n",
        "--number",
        help="number to encrypt",
    )
    parser.add_argument(
        "-f",
        "--file",
        help="path to image",
    )
    parser.add_argument(
        "-i",
        "--invisible",
        action="store_true",
        help="enable invisible mode",
    )

    args = parser.parse_args()

    if args.number and args.file:
        try:
            enc = np.encode(args.number)
            ip.encode(enc, args.file, args.invisible)
            print("Number encoded to " + args.file)
        except:
            print("Invalid number!")
    elif args.file:
        try:
            dec_img = ip.decode(args.file, args.invisible)
            dec_num = np.decode(dec_img)
            print(dec_num)
        except:
            print("Invalid image!")
    else:
        print("Invalid arguments!")
