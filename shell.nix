{ pkgs ? import <nixpkgs> { } }:

with pkgs;

mkShell {
  nativeBuildInputs = [
    python3
    python3Packages.numpy
    python3Packages.pillow
  ];
}
